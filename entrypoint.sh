#!/bin/bash
set -eo pipefail

# Used to stop kafka properly
function shutdown_kafka {
  echo "---> Stopping kafka..."
  kill $PIDS
  wait $PIDS
}

# Read some values form config file
zookeeper=$(cat ${KAFKA_HOME}/config/server.properties | grep zookeeper.connect= | awk -F= '{print $2}')
broker_id=$(cat ${KAFKA_HOME}/config/server.properties | grep broker.id= | awk -F= '{print $2}')

echo "---> Waiting for zookeeper to start"

while ! nc -z $zookeeper
do
  printf "."
  sleep 1
done

# Init healthcheck server in background
kafka-health-check \
  -check-interval 20s \
  -broker-host localhost \
  -broker-id $broker_id \
  -zookeeper $zookeeper &

# Start kafka in background
${KAFKA_HOME}/bin/kafka-server-start.sh ${KAFKA_HOME}/config/server.properties &

# Wait for kafka to start, then grab its pid
# Borrowed from kafka-stop-server.sh
echo "---> Waiting for kafka to start..."

until pids=$(ps ax | grep -i 'kafka' | grep java | grep -v grep | awk '{print $1}' | head -1)
do
  printf "."
  sleep 1
done

# Proxy SIGTERM to kafka
trap 'shutdown_kafka' SIGTERM

wait $pids
