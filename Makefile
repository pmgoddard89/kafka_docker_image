IMAGE_NAME = nathamanath/kafka_docker_image
VERSION = $(shell cat ./version.txt)
REGISTRY_URL=registry.gitlab.com

# this doesnt work if you arent nathan
# docker: docker.build docker.tag docker.push
docker: docker.build docker.tag


docker.build:
	docker build \
		--build-arg VERSION=$(VERSION) \
		--build-arg BUILD_DATE="$(shell date --rfc-3339=seconds)" \
		-t $(IMAGE_NAME) \
		.

docker.tag:
	docker tag $(IMAGE_NAME) $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION)

docker.push:
	docker push $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION)
