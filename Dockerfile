FROM anapsix/alpine-java:8_jdk

RUN apk update && apk add \
  wget \
  gnupg \
  curl

RUN rm -rf /var/cache/apk/*

ARG SCALA_VERSION=2.11
ARG KAFKA_VERSION=1.1.0

ENV KAFKA_HOME /opt/kafka

# Download and verify kafka
RUN wget -O /tmp/kafka.tar.gz http://apache.mirror.anlx.net/kafka/${KAFKA_VERSION}/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz
RUN wget -O /tmp/kafka.asc https://www.apache.org/dist/kafka/${KAFKA_VERSION}/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz.asc

# Verify kafka
RUN wget -q -O - https://kafka.apache.org/KEYS | gpg --import
RUN gpg --verify /tmp/kafka.asc /tmp/kafka.tar.gz

RUN tar -xvzf /tmp/kafka.tar.gz -C /tmp
RUN mv /tmp/kafka_${SCALA_VERSION}-${KAFKA_VERSION} /opt/kafka

# Healthcheck
RUN wget -O /tmp/healthcheck.tar.gz https://github.com/andreas-schroeder/kafka-health-check/releases/download/v0.0.2/kafka-health-check_0.0.2_linux_amd64.tar.gz
RUN tar -xvzf /tmp/healthcheck.tar.gz -C /usr/local/bin
COPY docker_healthcheck /usr/local/bin/docker_healthcheck
RUN chmod +x /usr/local/bin/docker_healthcheck
HEALTHCHECK CMD ["docker_healthcheck"]

# Tidy up
RUN apk del gnupg
RUN rm /tmp/kafka.tar.gz /tmp/kafka.asc /tmp/healthcheck.tar.gz

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

RUN adduser -D -H -s /bin/bash kafkauser
RUN mkdir /tmp/kafka-logs
RUN chown -R kafkauser /opt/kafka
RUN chown -R kafkauser /tmp/kafka-logs

USER kafkauser
WORKDIR /opt/kafka

ARG BUILD_DATE
ARG VERSION

LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name = "kafka"
LABEL org.label-schema.description = "apache kafka version ${KAFKA_VERSION} installed on alpine linux"
LABEL org.label-schema.vendor = "nathamanath"
LABEL org.label-schema.version = $VERSION
LABEL org.label-schema.schema-version = "1.0"

EXPOSE 9092

CMD entrypoint.sh
